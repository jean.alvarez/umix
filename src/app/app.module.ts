import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { PlaylistModule } from './features/playlist/playlist.module';
import { RouterStateSerializer, StoreRouterConnectingModule } from '@ngrx/router-store';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistEffects } from './features/playlist/store/playlist/effects/playlist.effects';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { routerReducers, CustomSerializer } from './store/reducers';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    StoreModule.forRoot(
      routerReducers
    ),
    StoreRouterConnectingModule.forRoot({ serializer: CustomSerializer}),
    EffectsModule.forRoot([PlaylistEffects]),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    PlaylistModule,
    BrowserAnimationsModule,
  ],
  providers: [{provide: RouterStateSerializer, useClass: CustomSerializer}],
  bootstrap: [AppComponent],
})
export class AppModule {}
