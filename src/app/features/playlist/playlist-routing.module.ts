import { Routes } from '@angular/router';
import {
  PlaylistRoutingPlaylist,
  PlaylistRoutingPlaylistContainer,
} from './playlist.model';

import { PlaylistContainerComponent } from './search/container/playlist-container/playlist-container.component';
import { PlaylistComponent } from './search/container/playlist/playlist.component';

export const routes: Routes = [
  {
    path: '',
    component: PlaylistComponent,
  },
  {
    path: PlaylistRoutingPlaylistContainer,
    component: PlaylistContainerComponent,
  },
  {
    path: '**',
    redirectTo: PlaylistRoutingPlaylist,
  },
];
