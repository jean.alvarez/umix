// Declaring routing constant for playlist module

// Route for playlist module
export const PlaylistRoutingSearchContainer = 'search-container';
export const PlaylistRoutingSearchList = 'search-list';
export const PlaylistRoutingSearchListItems = 'search-list-items';
export const PlaylistRoutingPlaylist = 'playlist';
export const PlaylistRoutingPlaylistContainer = ':playlistId';
export const PlaylistRoutingPlaylistItems = 'playlist-items';
export const PlaylistRoutingPlaylistDetails = ':playlistId/details';

