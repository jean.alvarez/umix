import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, filter, map, tap } from 'rxjs/operators';
import { Playlist } from '../../../playlist/playlist.model';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class PlaylistService {
  constructor(private httpClient: HttpClient) {}
  /**
   * Get playlist api url from environment.ts
   */
  PLAYLIST_API_URL = environment.apiPlaylists;

  /**
   * Method to handle error
   */
  static handleError(error: {
    error: { message: any };
    status: any;
    message: any;
  }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status} Message: ${error.message}`;
    }
    return throwError(errorMessage);
  }

  /**
   * Get all playlists from db.json
   */
  getAllPlaylists(): Observable<Playlist[]> {
    return this.httpClient
      .get<Playlist[]>(this.PLAYLIST_API_URL)
      .pipe(catchError(PlaylistService.handleError));
  }

  /**
   * Remove a playlist from db.json
   */
  deletePlaylist(id: number): any {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    const url = `${this.PLAYLIST_API_URL}/${id}`;
    return this.httpClient
      .delete<Playlist>(url, httpOptions)
      .pipe(catchError(PlaylistService.handleError));
  }
}
