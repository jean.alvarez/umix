export interface Playlist {
  id: number;
  name?: string;
  duration?: string;
  createdAt?: string;
  author?: string;
  isPlaying?: boolean;
  isLoading?: boolean;
  isLoaded?: boolean;
  songs?: Song[];
}

export interface Song {
  id: number;
  url?: string;
  name?: string;
  artist?: string;
  yearOfRelease?: string;
  duration?: string;
  selectedSongId?: number;
  isPlaying?: boolean;
  isEnded?: boolean;
}
