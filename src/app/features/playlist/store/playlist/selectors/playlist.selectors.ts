import { Route } from '@angular/compiler/src/core';
import {
  createSelector,
  createFeatureSelector,
  ActionReducerMap,
} from '@ngrx/store';
import * as fromRoute from '../../../../../store/reducers/index';
import { Playlist } from '../playlist.model';
import * as fromPlaylist from '../reducers/playlist.reducer';

export interface State {
  playlists: fromPlaylist.State;
}

export const reducers: ActionReducerMap<State> = {
  playlists: fromPlaylist.playlistReducers,
};

export const selectPlaylistState = createFeatureSelector<fromPlaylist.State>(
  fromPlaylist.playlistsFeatureKey
);

export const selectPlaylistIds = createSelector(
  selectPlaylistState,
  fromPlaylist.selectPlaylistIds // shorthand for playlistsState => fromplaylist.selectplaylistIds(playlistsState)
);

export const selectPlaylistEntities = createSelector(
  selectPlaylistState,
  fromPlaylist.selectPlaylistEntities
);

export const selectAllPlaylists = createSelector(
  selectPlaylistState,
  fromPlaylist.selectAllPlaylists
);

export const selectPlaylistTotal = createSelector(
  selectPlaylistState,
  fromPlaylist.selectPlaylistTotal
);


export const selectedPlaylist = createSelector(
  selectPlaylistEntities,
  fromRoute.selectRouter,
  (playlistEntities, router) => {
    return router.state && playlistEntities[router.state.params.playlistId];
  }
);

