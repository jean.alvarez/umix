import { createAction, props } from '@ngrx/store';
import { Update } from '@ngrx/entity';

import { Playlist } from './playlist.model';

export const loadPlaylists = createAction('[Playlist/API] Load Playlists');

export const loadPlaylistsSuccess = createAction(
  '[Playlist/API] Load Playlists Success',
  props<{ playlists: Playlist[] }>()
);

export const loadPlaylist = createAction(
  '[Playlist/API] Load Playlist',
  props<{ id: number }>()
);

export const addPlaylist = createAction(
  '[Playlist/API] Add Playlist',
  props<{ playlist: Playlist }>()
);

export const upsertPlaylist = createAction(
  '[Playlist/API] Upsert Playlist',
  props<{ playlist: Playlist }>()
);

export const addPlaylists = createAction(
  '[Playlist/API] Add Playlists',
  props<{ playlists: Playlist[] }>()
);

export const upsertPlaylists = createAction(
  '[Playlist/API] Upsert Playlists',
  props<{ playlists: Playlist[] }>()
);

export const updatePlaylist = createAction(
  '[Playlist/API] Update Playlist',
  props<{ playlist: Update<Playlist> }>()
);

export const updatePlaylists = createAction(
  '[Playlist/API] Update Playlists',
  props<{ playlists: Update<Playlist>[] }>()
);

export const deletePlaylist = createAction(
  '[Playlist/API] Delete Playlist',
  props<{ id: number }>()
);

export const deletePlaylists = createAction(
  '[Playlist/API] Delete Playlists',
  props<{ ids: string[] }>()
);

export const clearPlaylists = createAction('[Playlist/API] Clear Playlists');
