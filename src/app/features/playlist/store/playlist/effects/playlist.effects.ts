import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { mergeMap, map, catchError, tap, concatMap } from 'rxjs/operators';
import { PlaylistService } from '../../shared/services/playlist/playlist.service';
import * as playlistsActions from '../../playlist/playlist.actions';
@Injectable()
export class PlaylistEffects {
  constructor(
    private actions$: Actions,
    private playlistService: PlaylistService
  ) {}
  loadPlaylists$ = createEffect(() =>
     this.actions$.pipe(
      ofType('[Playlist/API] Load Playlists'),
      mergeMap(() =>
        this.playlistService.getAllPlaylists().pipe(
          map((playlists) => {
            console.log();
            return playlistsActions.loadPlaylistsSuccess({playlists});
          }),
          catchError(() => EMPTY)
        )
      )
    )
  );

    deletePlaylist$ = createEffect(() =>
    this.actions$.pipe(
      ofType(playlistsActions.deletePlaylist),
      concatMap((action) => this.playlistService.deletePlaylist(action.id))
    ),
    {dispatch: false}
  );
}
