import { createFeatureSelector, createReducer, on } from '@ngrx/store';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Playlist, Song } from '../playlist.model';
import * as PlaylistActions from '../playlist.actions';
import * as fromRouter from '@ngrx/router-store';

export const playlistsFeatureKey = 'playlists';

export interface State extends EntityState<Playlist> {}

export const adapter: EntityAdapter<Playlist> = createEntityAdapter<Playlist>();

export const initialState: State = adapter.getInitialState({});

export const playlistReducers = createReducer(
  initialState,
  on(PlaylistActions.addPlaylist, (state, action) =>
    adapter.addOne(action.playlist, state)
  ),
  on(PlaylistActions.upsertPlaylist, (state, action) =>
    adapter.upsertOne(action.playlist, state)
  ),
  on(PlaylistActions.addPlaylists, (state, action) =>
    adapter.addMany(action.playlists, state)
  ),
  on(PlaylistActions.upsertPlaylists, (state, action) =>
    adapter.upsertMany(action.playlists, state)
  ),
  on(PlaylistActions.updatePlaylist, (state, action) =>
    adapter.updateOne(action.playlist, state)
  ),
  on(PlaylistActions.updatePlaylists, (state, action) =>
    adapter.updateMany(action.playlists, state)
  ),
  on(PlaylistActions.deletePlaylist, (state, action) =>
    adapter.removeOne(action.id, state)
  ),
  on(PlaylistActions.deletePlaylists, (state, action) =>
    adapter.removeMany(action.ids, state)
  ),
  on(PlaylistActions.loadPlaylistsSuccess, (state, action) =>
    adapter.setAll(action.playlists, state)
  ),
  on(PlaylistActions.clearPlaylists, (state) => adapter.removeAll(state))
);

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

// get selected playlist Id

// select the array of playlist ids
export const selectPlaylistIds = selectIds;

// select the dictionary of playlist entities
export const selectPlaylistEntities = selectEntities;

// select the array of playlists
export const selectAllPlaylists = selectAll;

// select the total playlist count
export const selectPlaylistTotal = selectTotal;
