import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SearchContainerComponent } from './search/container/search-container/search-container.component';
import { SearchListComponent } from './search/components/search-list/search-list.component';
import { SearchListItemsComponent } from './search/components/search-list-items/search-list-items.component';
import { routes } from './playlist-routing.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { PlaylistEffects } from './store/playlist/effects/playlist.effects';
import * as FromReducers from './store/playlist/reducers/playlist.reducer';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'src/app/shared/shared.module';
import { MaterialModule } from 'src/app/shared/material.module';
import { PlaylistComponent } from './search/container/playlist/playlist.component';
import { PlaylistItemComponent } from './search/components/playlist-item/playlist-item.component';
import { PlaylistDetailsComponent } from 'src/app/features/playlist/search/components/playlist-details/playlist-details.component';
import { PlaylistContainerComponent } from './search/container/playlist-container/playlist-container.component';

@NgModule({
  declarations: [
    PlaylistComponent,
    PlaylistItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent,
    SearchContainerComponent,
    SearchListComponent,
    SearchListItemsComponent,
  ],
  imports: [
    // Common modules
    CommonModule,
    SharedModule,
    // Material modules
    MaterialModule,
    // Router module
    RouterModule.forChild(routes),
    // Store module
    StoreModule.forFeature(
      FromReducers.playlistsFeatureKey,
      FromReducers.playlistReducers
    ),
    EffectsModule.forFeature([PlaylistEffects]),
    // http client module
    HttpClientModule,
  ],
})
export class PlaylistModule {}
