import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'u-playlist-search-list-items',
  templateUrl: './search-list-items.component.html',
  styleUrls: ['./search-list-items.component.css']
})
export class SearchListItemsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
