import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'u-playlist-search-list',
  templateUrl: './search-list.component.html',
  styleUrls: ['./search-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchListComponent {}
