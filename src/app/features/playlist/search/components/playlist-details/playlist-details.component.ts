import { Component, Input } from '@angular/core';
import { Playlist } from '../../../store/playlist/playlist.model';

@Component({
  selector: 'u-playlist-details',
  templateUrl: './playlist-details.component.html',
  styleUrls: ['./playlist-details.component.css'],
})
export class PlaylistDetailsComponent {
  /**
   * The type for playlist input
   */
  @Input() playlist: Playlist | null | undefined;

  /**
   * Implementing track by id function for optimize purpose
   */
  trackById(item: any): string {
    return item.id;
  }

  /**
   * Removing playlist item
   */
  deleteItem(event: any) {
    console.log(event);
    // Dispatch action to remove playlists from state
    // this.store.dispatch(fromActions.deleteSong({ id: event }));
  }
}
