import {
  Component,
  Input,
  Output,
  ChangeDetectionStrategy,
  EventEmitter,
} from '@angular/core';
import { Playlist } from '../../../store/playlist/playlist.model';
import { PlaylistRoutingPlaylist } from '../../../playlist.model';
import { Store } from '@ngrx/store';
import * as fromPlaylist from '../../../store/playlist/reducers/playlist.reducer';
import * as fromActions from '../../../store/playlist/playlist.actions';

@Component({
  selector: 'u-playlist-item',
  templateUrl: './playlist-item.component.html',
  styleUrls: ['./playlist-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PlaylistItemComponent {
  constructor(private store: Store<fromPlaylist.State>) {}

  /**
   * The type for playlist input
   */
  @Input() playlist: Playlist;
  /**
   * The type for route input
   */
  @Input() route: string = PlaylistRoutingPlaylist;

  /**
   * Removing playlist item
   */
  deleteItem(event: any) {
    const remove = window.confirm('Are you sure?');
    if (remove) {
      // Dispatch action to remove playlists from state
      this.store.dispatch(fromActions.deletePlaylist({ id: event }));
    }
  }
}
