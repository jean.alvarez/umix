import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Playlist } from '../../../store/playlist/playlist.model';
import { Store } from '@ngrx/store';
import * as fromSelectors from '../../../store/playlist/selectors/playlist.selectors';
import * as fromRoute from '../../../../../store/reducers/index';
import * as fromAction from '../../../../playlist/store/playlist/playlist.actions';

@Component({
  selector: 'u-playlist-container',
  templateUrl: './playlist-container.component.html',
  styleUrls: ['./playlist-container.component.scss'],
})
export class PlaylistContainerComponent implements OnInit {
  /**
   * Provide playlist observable to template
   */
  playlist$!: Observable<Playlist | undefined>;
  /**
   * Provide playlist item observable to template
   */
  playlistItems$!: Observable<Playlist[] | undefined>;

  constructor(private store: Store<fromRoute.State>) {}

  ngOnInit() {
    this.playlist$ = this.store.select(fromSelectors.selectedPlaylist);
  }
}
