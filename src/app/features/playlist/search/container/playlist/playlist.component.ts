import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Playlist } from '../../../store/playlist/playlist.model';
import * as fromPlaylist from '../../../store/playlist/reducers/playlist.reducer';
import * as selectors from '../../../store/playlist/selectors/playlist.selectors';
import * as fromActions from '../../../store/playlist/playlist.actions';

@Component({
  selector: 'u-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.scss'],
})
export class PlaylistComponent implements OnInit {
  /**
   * Provide playlists  observable to template
   */
  playlists$!: Observable<Playlist[]>;
  /**
   * Provide current playlist item observable to template
   */
  currentPlaylist$!: Observable<Playlist | undefined>;

  constructor(private store: Store<fromPlaylist.State>) {}

  ngOnInit(): void {
    // Dispatch action to load playlists from state
    this.store.dispatch(fromActions.loadPlaylists());
    // Display loaded playlists to template
    this.playlists$ = this.store.select(selectors.selectAllPlaylists);
  }
}
