import { ChangeDetectionStrategy } from '@angular/core';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Song } from 'src/app/features/playlist/store/playlist/playlist.model';

@Component({
  selector: 'u-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListItemComponent {
  /**
   * Declaring event emitter output for deleting playlist item
   */
  @Output()
  deleteItem: EventEmitter<number> = new EventEmitter<number>();

  /**
   * The type for playlist input
   */
  @Input() item?: Song | null | undefined;

  /**
   * Emit event on delete item
   */
  deleteItemId(id: number | undefined) {
    this.deleteItem.emit(id);
  }
}
