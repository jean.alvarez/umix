import { ChangeDetectionStrategy } from '@angular/core';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import {
  Playlist,
  Song,
} from 'src/app/features/playlist/store/playlist/playlist.model';

@Component({
  selector: 'u-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CardComponent {
  /**
   * Declaring event emitter output for deleting playlist item
   */
  @Output()
  deletePlaylistItem: EventEmitter<number> = new EventEmitter<number>();
  /**
   * The type for itemId input
   */
  @Input() itemId?: number | undefined;
  /**
   * The type for route input
   */
  @Input() route?: string | null | undefined;
  /**
   * The type for title input
   */
  @Input() title?: string | null | undefined;
  /**
   * The type for subtitle input
   */
  @Input() subTitle?: string | null | undefined;
  /**
   * The type for list input
   */
  @Input() list?: Song[] | null | undefined;
  /**
   * Implementing track by id function for optimize purpose
   */
  trackById(item: any): string {
    return item.id;
  }

  deleteItem(id: number | undefined) {
    this.deletePlaylistItem.emit(id);
  }
}
