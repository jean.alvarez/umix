import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';

import {
  ErrorStateMatcher,
  ShowOnDirtyErrorStateMatcher,
} from '@angular/material/core';

import { DomSanitizer } from '@angular/platform-browser';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';
/**
 * Custom icons added to the material icons' registry
 * The icons must be in SVG format and the key is always prefixed by "mm-"
 */
const CUSTOM_ICONS = {
  'u-play': 'play.svg',
  'u-pause': 'pause.svg',
  'u-delete': 'delete.svg',
};

/**
 * Material modules needed for the app
 */
const MATERIAL = [
  MatCardModule,
  MatIconModule,
  FlexLayoutModule,
  MatButtonModule,
];

/**
 * Imports Material modules
 * Add Custom Icons to MatIconsRegistry
 * Configuration for Date Format
 */
@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule, ...MATERIAL],
  exports: MATERIAL,
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
  ],
})
export class MaterialModule {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    for (const [name, path] of Object.entries(CUSTOM_ICONS)) {
      this.matIconRegistry.addSvgIcon(
        name,
        this.domSanitizer.bypassSecurityTrustResourceUrl(
          '/assets/img/icons/' + path
        )
      );
    }
  }
}
