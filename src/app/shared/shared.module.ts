import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CardComponent } from './components/card/card.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { MaterialModule } from './material.module';

const SHARED_COMPONENTS = [CardComponent, ListItemComponent];

// const SHARED_PIPES = [DatePipe];

const SHARED = [...SHARED_COMPONENTS];

@NgModule({
  declarations: SHARED,
  imports: [CommonModule, MaterialModule, RouterModule],
  exports: SHARED,
  entryComponents: [CardComponent],
  providers: [DatePipe],
})
export class SharedModule {}
