import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRoutingPlaylist } from './app.model';

const routes: Routes = [
  {
    path: '',
    redirectTo: AppRoutingPlaylist,
    pathMatch: 'full',
  },
  {
    path: AppRoutingPlaylist,
    loadChildren: () => import('./features/playlist/playlist.module').then(m => m.PlaylistModule),
  },
  {
    path: '**',
    redirectTo: AppRoutingPlaylist,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
